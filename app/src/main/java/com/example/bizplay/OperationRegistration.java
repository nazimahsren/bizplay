package com.example.bizplay;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.example.bizplay.databinding.ActivityOperationRegistrationBinding;
import com.example.bizplay.dialog.CustomDateDialog;
import com.example.bizplay.dialog.CustomImageDialog;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class OperationRegistration extends AppCompatActivity implements CustomImageDialog.OnImageClicked {

    private static final int SELECT_PICTURE = 100;
    private ActivityOperationRegistrationBinding binding;


    private CustomImageDialog customImageDialog;

    private ImageView mCancel;
    private TextView mDate;
    private Button btnOk, btnCancel;
    private ImageView mPic, mResult;
    private LinearLayout linearLayout;
    private Context context;
    private Bitmap bitmap;
    private Spinner spinner;


    String [] items = {"차량정보","배차정보"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_operation_registration);

        mCancel = binding.ivCancel;
        mDate = binding.tvDate;
        mPic = binding.ivPic;
        mResult = binding.ivPicResult;
        spinner = binding.spinner;


        mCancel.setOnClickListener(v -> {
            Intent intent = new Intent(OperationRegistration.this, OperationActivity.class);
            startActivity(intent);
        });

        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        SimpleDateFormat format = new SimpleDateFormat("yyyy '년' MM '월' dd'일'", Locale.KOREA);
        final TextView txtDate = binding.tvDate;
        String dt = format.format(new Date());
        txtDate.setText(dt);

        final CustomDateDialog customDateDialog = new CustomDateDialog(this, new CustomDateDialog.OnDateClicked() {
            @Override
            public void onClick(Date date) {
                txtDate.setText(format.format(date));
            }
        });
        txtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDateDialog.show();
            }
        });


        final CustomImageDialog dialog = new CustomImageDialog(this);
        mPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show(getSupportFragmentManager(), "TAG");
            }
        });
    }

    @Override
    public void onImageSelectFromGallery(Uri uri) {
         mResult.setVisibility(View.VISIBLE);
        mResult.setImageURI(uri);
    }

    @Override
    public void onImageSelectFromCamera(Bitmap bitmap) {
        mResult.setVisibility(View.VISIBLE);
        mResult.setImageBitmap(bitmap);
    }




}
