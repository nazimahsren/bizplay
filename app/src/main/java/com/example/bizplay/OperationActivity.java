package com.example.bizplay;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.example.bizplay.databinding.ActivityOperationBinding;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

public class OperationActivity extends AppCompatActivity {

    ActivityOperationBinding binding;

    private FloatingActionMenu fab_menu;
    private View view;

    private FloatingActionButton mRequestCar, mAddReport;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_operation);

        fab_menu = binding.fabMenu;
        view = binding.viewBackground;
        mAddReport = binding.btnAddReport;
        mRequestCar = binding.btnRequestCar;


        fab_menu.setOnMenuToggleListener(new FloatingActionMenu.OnMenuToggleListener() {
            @Override
            public void onMenuToggle(boolean opened) {
                if (opened) {

                    view.setVisibility(View.VISIBLE);

                } else {
                    view.setVisibility(View.GONE);
                }
            }
        });


        mAddReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OperationActivity.this, OperationRegistration.class);
                startActivity(i);
            }
        });


    }
}



