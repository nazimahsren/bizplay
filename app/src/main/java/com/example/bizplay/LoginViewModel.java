package com.example.bizplay;

import android.view.View;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class LoginViewModel extends ViewModel {

    public LoginActivity loginActivity;
    public MutableLiveData<String> Id = new MutableLiveData<>();
    public MutableLiveData<String> Password = new MutableLiveData<>();

    private MutableLiveData<LoginModel> loginModelMutableLiveData;

    public MutableLiveData<LoginModel> getLogin(){

        if (loginModelMutableLiveData == null){
            loginModelMutableLiveData = new MutableLiveData<>();
        }
        return loginModelMutableLiveData;
    }
    public void onClickLogin(View v){
        LoginModel loginModel = new LoginModel(Id.getValue(), Password.getValue());
        loginModelMutableLiveData.setValue(loginModel);
    }

}
