package com.example.bizplay.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.Window;
import android.widget.DatePicker;

import com.example.bizplay.databinding.DateLayoutBinding;

import java.util.Calendar;
import java.util.Date;

public class CustomDateDialog extends Dialog {

    private DateLayoutBinding binding;
    private Calendar calendar;

    public CustomDateDialog(Context context, final OnDateClicked clickListener){
        super(context);

        binding = DateLayoutBinding.inflate(getLayoutInflater());

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(binding.getRoot());
        this.setCancelable(true);

//        calendar = Calendar.getInstance();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            binding.datePicker.setOnDateChangedListener((view, year, monthOfYear, dayOfMonth) -> {
//
//                calendar.set(Calendar.MONTH, monthOfYear);
//                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//                calendar.set(Calendar.YEAR, year);
//            });
//        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        binding.datePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {

            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {

                calendar.set(Calendar.MONTH, (month));
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                calendar.set(Calendar.YEAR, year);
            }
        });

        binding.btnOk.setOnClickListener(v -> {
            Log.d("test::", "calendar_getTime::" +calendar.getTime());
            clickListener.onClick(calendar.getTime());
            dismiss();
        });
        binding.btnCancel.setOnClickListener(v -> dismiss());
    }

        public interface OnDateClicked{

        void onClick (Date date);

        }
}
