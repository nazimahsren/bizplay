package com.example.bizplay.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.example.bizplay.Gallery;
import com.example.bizplay.OperationRegistration;
import com.example.bizplay.R;
import com.example.bizplay.databinding.SelectPhotoDialogBinding;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import static android.app.Activity.RESULT_OK;


public class CustomImageDialog extends BottomSheetDialogFragment {

    public static final int SELECT_PICTURE_GALLERY = 100;
    public static final int SELECT_PICTURE_CAMERA = 200;

    SelectPhotoDialogBinding binding;

    private Context context;
    private LinearLayout mTakePic, mChoosePick;
    private OnImageClicked onImageClicked;


    public CustomImageDialog(Context context) {
        onImageClicked = (OnImageClicked) context;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.select_photo_dialog, null, false);

        View view = binding.getRoot();
        mTakePic = binding.lnlTakePic;
        mChoosePick = binding.lnlChoosePic;


        mTakePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent takepicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takepicture, SELECT_PICTURE_CAMERA);

            }
        });

        mChoosePick.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
               Intent intent = new Intent(getActivity(),Gallery.class);
               startActivity(intent);

            }
        });
        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case SELECT_PICTURE_CAMERA:
                if (resultCode == RESULT_OK) {
                    Bundle extras = data.getExtras();
                    Bitmap bitmap = (Bitmap) extras.get("data");
                    onImageClicked.onImageSelectFromCamera(bitmap);
                }
                break;
            case SELECT_PICTURE_GALLERY:
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
                    onImageClicked.onImageSelectFromGallery(uri);
                }
                break;
        }
        dismiss();
    }


    public interface OnImageClicked {
        void onImageSelectFromGallery(Uri uri);

        void onImageSelectFromCamera(Bitmap uri);
    }
}
