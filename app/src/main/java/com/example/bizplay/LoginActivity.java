package com.example.bizplay;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.bizplay.databinding.ActivityLoginBinding;

import java.util.Objects;

public class LoginActivity extends AppCompatActivity {

    private ActivityLoginBinding binding;
    boolean isCheck = true;

    private LoginViewModel loginViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        final EditText id = binding.edtId;
        final EditText pass = binding.edtPassword;
        final Button btn_login = binding.btnLogin;
        final ImageView eye_hint = binding.ivHintEye;

        binding.setLifecycleOwner(this);
        binding.setLoginViewModel(loginViewModel);




        loginViewModel.getLogin().observe(this,(LoginModel loginModel) -> {



            if (TextUtils.isEmpty(Objects.requireNonNull(loginModel).getLoginId())){
                binding.edtId.setError("Please enter your Id");
                binding.edtId.requestFocus();
            }
            else if (TextUtils.isEmpty(Objects.requireNonNull(loginModel).getLoginPassword())){
                binding.edtPassword.setError("Please enter your Password");
                binding.edtPassword.requestFocus();
            }
            btn_login.setOnClickListener(v -> {
                Intent i = new Intent(LoginActivity.this, OperationActivity.class);
                startActivity(i);
            });
            });
        



        pass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().isEmpty()){
                    eye_hint.setVisibility(View.VISIBLE);
                }else eye_hint.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        eye_hint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCheck){
                    isCheck = false;
                    eye_hint.setBackground(getDrawable(R.drawable.pwd_eyes_on));
                    pass.setTransformationMethod(null);
                }else {
                    isCheck = true;
                    eye_hint.setBackground(getDrawable(R.drawable.pwd_eyes_off));
                pass.setTransformationMethod(new PasswordTransformationMethod());
                }
                pass.setSelection(pass.getText().toString().length());
            }

        });

    }
}
