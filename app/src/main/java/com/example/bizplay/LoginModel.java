package com.example.bizplay;

import android.text.TextUtils;

class LoginModel {

    private String loginId;
    private String loginPassword;

    public LoginModel(String loginId, String loginPassword) {
        this.loginId = loginId;
        this.loginPassword = loginPassword;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public boolean isValidId(){
        if (this.loginId != null && !TextUtils.isEmpty(loginId)){
            return true;
        }
        return false;
    }
}
